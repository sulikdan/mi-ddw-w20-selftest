---
geometry: margin=0.66in
fontsize: 9pt
header-includes:
    - \setlength{\parskip}{0.66in}
---

# MI-DDW 2017/18 Self-test sheet na teorii

Co je text mining a popište jednotlivé kroky

Co je disambiguation a uvést příklad

Popište Coreference Resolution.

Jaký je rozdíl mezi normálním a invertováným indexem?

Jak lze zkomprimovat index? Uveďte 3 metody.

Jak zajistí Google matice v kontextu pagerank neuváznutí?

Uveďte co je a k čemu slouží tzv. „Reservoir Sampling“. 

Jak řešit při crawlování URI odkazující na stejný zdroj?

Co jsou Motifs a kde se využívají?

Popište, jaké jsou výhody a nevýhody použití doporučovacích systémů.

Popište metriky doporučovacích systémů.

Co je to Deep web a proč je problém pro crawlery?

Vysvětlete k čemu slouží metoda Girvan-Newman a popište základní kroky. 

Vysvětlete pojem „neredukovatelný“ (irreducible) v kontextu algoritmu PageRank. Jak je to řešeno v Google matici? 

Co je to "reciprocity"?

Co je clique percolation?

Co je NER a popište postup.

Stemming, Lemmatization - popsat a příklad.

Vysvětlete, co je to normalizace URL a kde se využívá. Dále uveďte minimálně dva příklady.

Vysvětlete, jakým způsobem se může identifikovat robot (crawler). Uveďte příklad, jaký má identifikace význam? 

Popište Lossy Counting v dolování streamů.

Uveďte jak vypadá výpočet TF-IDF a popište jej.

Zadefinujte jednou větou následující pojmy: Precision, Recall, Accuracy, F-measure

Co je inverted index, výhody a nevýhody?

Popište, co je to NIF.

Co je to Bounce rate?

Popište postup WUM.

Uveďte 3 klady a zápory Collaborative Filltering.

Co jsou to sitemaps a jakou mají strukturu?

Popište hub a autoritu v HITS.

Vysvětlete fenomén malého světa („small world phenomenon“) a uveďte libovolný příklad.

Uveďte základní rozdíl mezi implicitními a explicitními daty používanými při WUM. 

Který z následujících přístupů kolaborativního filtrování bývá výpočetně méně náročný - „user-based“ nebo „item-based“? Svou odpověď zdůvodněte.

Uveďte 2+ Attacks on collaborative filtering.

Co jsou sketches v data streams?

Chceme ukládat a indexovat dokumenty, jak to uděláme, aby v tom šlo efektivně vyhledávat? Uveďte výhody a nevýhody.

Jak porovnávat indexy a query při vyhledávání?

Co je to hashování indexu? Uvěďte 3 výhody či nevýhody.

Jak lze rozpoznat uživatele?

Popište, co je to Parts of Speech a uveďte příklad.

Co to je konverzní trychtýř?

Co jsou HITS a jak se počítají?

Popište Apriori algoritmus.

Popište podrobně Kernighan-Lin.

Uveďte 3+ metriky pro porovnání důležitosti uzlu.

Popište Bow-tie.

Definujte termíny "seed page", "frontier" a "fetcher" z terminologie crawlingu.

Uveďte dvě strategie crawlování.

Definujte homofilii (homophily).

Uveďte 3 typy počítání frekvencí při data streams. Jeden z nich popište.

Jak jinak lze roboty žádat o změny v chování, kromě souboru `robots.txt`?

Co to je crawler/spider past?

Co je Structure Hole?

Popište rozdíl mezi Information Retrieval a Recommending System.

Jak lze predikovat další hranu v grafu sítě? Uveďte jeden způsob.

Uveďte tři vstupní data WUM.

Co je to tokenizace v kontextu text miningu?

Popište výpočet Edge Betweeness, ukažte první tři iterace.

Co je Preferential Attachment?

Co je to opinion mining? Uveďte dva typy.

Definujte clustering coefficient a bridge.

Co jsou to matice UPM (user-pageview matrix), PFM (pageview-feature matrix) a TFM (transaction-feature matrix)?

Jak lze testovat kvalitu Recommender systems?

Čím snížíme dimenzionalitu textu při text miningu?

Jak se lze bránit útokům na CF?

Uveďte 3 typy samplingu data streams.

Uveďte dva modely Information Retrieval.