# MI-DDW & MI-W20 2018 Self-test Sheet Combo

This is a self-test sheet for easy memorization of MI-DDW and MI-W20 Topics.

Generated PDFs can be found in [releases](https://gitlab.fit.cvut.cz/hlavam30/mi-ddw-w20-selftest/tags).

## How to build

Converting to PDF with pandoc is enough. Depending on which document you want, run:

```
$ pandoc ddw-selftest.md -o ddw-selftest.pdf
$ pandoc ddw-selftest-results.md -o ddw-selftest-results.pdf
```

or

```
$ pandoc w20-selftest.md -o w20-selftest.pdf
$ pandoc w20-selftest-results.md -o w20-selftest-results.pdf
```

or run that super-awesome-complex-complicated script

```
$ ./justdoit.sh
```

## Recommended procedure

Create/download PDFs, print it out, test yourself off-line (no help, no cheating, no interwebz) and compare your results.

## Mistakes, error

It is, probably, full of mistakes. If you find any, don't be afraid to send a merge request.